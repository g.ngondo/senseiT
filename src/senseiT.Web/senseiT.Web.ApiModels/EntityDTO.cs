
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using senseiT.Core.Entities;
using senseiT.Core.Shared;

namespace senseiT.Web.ApiModels
{
    // Note: doesn't expose events or behavior
    public class EntityDTO
    {
        
        public Guid Id {get; set;}
        public string Name { get; set; }
        public EntityType Type {get;set;}
        public string Url { get; set; }    
        public int Level {get;set;}
        public int Weight {get;set;} = 0;
        public List<EntityDTO> LinkedEntities {get;set;} = new List<EntityDTO>();
        public static EntityDTO FromEntityItem(Entity item)
        {
            EntityDTO newEntity = new EntityDTO();
            newEntity.Id = item.Id;
            newEntity.Name = item.Name;
            newEntity.Type = item.Type;
            newEntity.Url = item.Url;
            newEntity.Level = item.Level;
            newEntity.Weight = item.Weight;
            if (item.LinkedEntities != null)
            {
                newEntity.LinkedEntities = item.LinkedEntities.Select(ent => EntityDTO.FromEntityItem(ent)).ToList();
            }
            return newEntity;
        }

        public static Entity ToEntityItem(EntityDTO item)
        {
            Entity newEntity = new Entity();
            newEntity.Id = item.Id;
            newEntity.Name = item.Name;
            newEntity.Type = item.Type;
            newEntity.Url = item.Url;
            newEntity.Level = item.Level;
            newEntity.Weight = item.Weight;
            if (item.LinkedEntities != null)
            {
                newEntity.LinkedEntities = item.LinkedEntities.Select(ent => EntityDTO.ToEntityItem(ent)).ToList();
            }
            return newEntity;
        }
        
    }
}
