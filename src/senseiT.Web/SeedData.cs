using System.Net.Http;
using System.Threading.Tasks;
using senseiT.Core.Entities;
using senseiT.Core.Shared;
using senseiT.Infrastructure.Data;
using Microsoft.AspNetCore.JsonPatch;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Collections;
using System;

namespace senseiT.Web
{
    public static class SeedData
    {
        public async static void PopulateTestData(AppDbContext dbContext)
        {
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
         
            Entity scrapped = await ScrapAwesomeLists("https://raw.githubusercontent.com/lockys/awesome.json/master/awesome/awesome.json");
            
            Entity scrapped2 = await ScrapAwesomeLists("https://raw.githubusercontent.com/lockys/awesome.json/master/repo-json/sindresorhus-awesome-nodejs.json");
            scrapped2.Name="Node.js";

            scrapped.UpdateOneBy(entity => {
                return entity.Name == "Node.js";
            }, scrapped2);

            Entity scrapped3 = await ScrapAwesomeLists("https://raw.githubusercontent.com/lockys/awesome.json/master/repo-json/sorrycc-awesome-javascript.json");
            scrapped3.Name="JavaScript";
            
            scrapped.UpdateOneBy(entity => {
                return entity.Name == "JavaScript";
            }, scrapped3);

            if (scrapped != null)
            {
                dbContext.Add(scrapped);
                dbContext.SaveChanges();
            }
            

            

        }
        static bool IsValidJson(string json)
        {
            try {
                JToken.Parse(json);
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }
        static async Task<Entity> ScrapAwesomeLists(string url)
        {
            Entity result = new Entity();
            string resultFromUrl = await GetAwesomeListJson(url);
            //Remove all newlines
            resultFromUrl = ReplaceNewLines(resultFromUrl);

            if (!IsValidJson(resultFromUrl))
                return null;
            //Transform Json into enumerable object
            
            JToken JsonObj = JToken.Parse(resultFromUrl);
           
            
            Stack stack = new Stack();
            stack.Push(JsonObj);
           
            //Loop through nodes & construct entity model.
            //Once completed, add the new entity into db
            //Prepare root Element
            int level = 0;
            do {
                JToken tok = (JToken)stack.Pop();
                Entity parentNodeinlist = result.FindOneBy(ent => {
                    return ent.Name == tok.Path;
                });
                if (tok.Type == JTokenType.Object && tok["name"] == null)
                {
                    foreach (JProperty child in tok.Children<JProperty>())
                    {
                        Entity newEntity = new Entity();
                        newEntity.Name = child.Path.Trim();
                        newEntity.Type = EntityType.Category;
                        result.AddLinkedEntity(newEntity);
                        stack.Push(child.Value);
                    }
                }
                if (tok.Type == JTokenType.Array && parentNodeinlist != null)
                {
                    foreach (JToken child in tok.Children())
                    {
                        Entity newEntity = new Entity();
                        newEntity.Name = (string)child["name"];
                        newEntity.Url = (string)child["url"];
                        
                        parentNodeinlist.AddLinkedEntity(newEntity);

                       /* result.UpdateOneBy(ent => {
                            return ent.Id == parentNodeinlist.Id;
                        }, newEntity);*/
                    }
                    level++;
                }

                if (tok.Type == JTokenType.Array && parentNodeinlist == null)
                {
                    foreach (JToken child in tok.Children())
                    {
                        Entity newEntity = new Entity();
                        newEntity.Name = (string)child["name"];
                        newEntity.Url = (string)child["url"];

                        result.AddLinkedEntity(newEntity);
                    }
                }
                
            }
            while (stack.Count > 0);
           
            return result;
            
        }
        static string ReplaceNewLines(string text)
        {
           text = text.Replace("\n", "");
           text = text.Replace("\r\n", "");
           return text;
        }

        private async static Task<string> GetAwesomeListJson(string url)
        {
            HttpClient client = new HttpClient();
            
            var response = await client.GetAsync(url);
            return await response.Content.ReadAsStringAsync();
        }

    }
}
