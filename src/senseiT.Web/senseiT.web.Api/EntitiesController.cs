
using System;
using System.Threading.Tasks;
using senseiT.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using senseiT.Core.Entities;
using senseiT.Web.ApiModels;
using System.Linq;

namespace senseiT.Web.Api
{
    [Route("api/[controller]")]
    public class EntityController : Controller
    {
        IRepository<Entity> entityRepository;
        public EntityController(IRepository<Entity> entityRepository) {
            this.entityRepository = entityRepository;
        }

        [HttpGet]
        public async Task<IActionResult> List() 
        {
            var items = entityRepository.List()
                            .Select(item => EntityDTO.FromEntityItem(item));
            return await Task.FromResult(Ok(items));
        }

          // GET: api/Entity
        [HttpGet("{id:Guid}")]
        public IActionResult GetById(Guid id)
        {
            var item = entityRepository.GetById(id);
            if (item != null)
                return Ok(item);
            return BadRequest("Item not found");
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]EntityDTO item)
        {
            var newentity = EntityDTO.ToEntityItem(item);
            return await Task.FromResult(Ok(entityRepository.Add(newentity)));
        }

        [HttpPut]
        public IActionResult Put([FromBody]Entity item)
        {
            var toUpdate = entityRepository.GetById(item.Id);
            if (toUpdate != null) {
                toUpdate.Type = item.Type;
                toUpdate.LinkedEntities = item.LinkedEntities;
            }
            entityRepository.Update(toUpdate);
            return Ok(EntityDTO.FromEntityItem(toUpdate));
        }

       
       


    }
    
}