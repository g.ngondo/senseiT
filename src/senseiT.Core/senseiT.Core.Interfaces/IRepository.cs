using System;
using System.Collections.Generic;
using senseiT.Core.Shared;

namespace senseiT.Core.Interfaces
{
    public interface IRepository<T> where T : BaseEntity
    {
        T GetById(Guid id);
        List<T> List();
        T Add(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}