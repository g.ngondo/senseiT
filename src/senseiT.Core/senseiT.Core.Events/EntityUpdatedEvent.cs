using senseiT.Core.Entities;
using senseiT.Core.Shared;

namespace  senseiT.Core.Events
{
    public class EntityUpdatedEvent : BaseDomainEvent
    {
        public Entity UpdatedEntity {get;set;}
        public EntityUpdatedEvent(Entity updatedEntity)
        {
            UpdatedEntity = updatedEntity;
        }
    }
}