using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using senseiT.Core.Events;
using senseiT.Core.Shared;

namespace senseiT.Core.Entities
{
    public class Entity : BaseEntity
    {
        public Entity() : base(){
            LinkedEntities = new List<Entity>();
        }
        public override string ToString() {
            return this.Name;
        }
        public void AddLinkedEntity(Entity entity)
        {
            Stack stack = new Stack();
            

            stack.Push(entity);

            while(stack.Count > 0)
            {
                Entity newentity = (Entity) stack.Pop();
                newentity.Level = this.Level + 1;

                foreach (Entity child in newentity.LinkedEntities)
                {
                    child.Level = newentity.Level + 1;
                    
                    stack.Push(child);
                }
                entity.UpdateOneBy(ent => { return ent.Id == newentity.Id; } ,newentity);
            }
            LinkedEntities.Add(entity);
        }

        public void AddListEntity(List<Entity> list)
        {
            foreach (Entity ent in list)
            {
                AddLinkedEntity(ent);
            }
        }
       

        public void UpdateOneBy(Predicate<Entity> predicate, Entity newEntity)
        {
            Stack stack = new Stack();
            stack.Push(this);
            while(stack.Count > 0)
            {
                Entity ent = (Entity)stack.Pop();

                for (int i = 0; i < ent.LinkedEntities.Count; i++)
                {
                    if (predicate(ent.LinkedEntities[i]))
                    {
                        
                        ent.LinkedEntities.RemoveAt(i);
                        ent.AddLinkedEntity(newEntity);
                        return;
                    }
                    stack.Push(ent.LinkedEntities[i]);
                }
            }
        }

        public Entity FindOneBy(Predicate<Entity> predicate)
        {
            Stack stack = new Stack();
            stack.Push(this);

            while(stack.Count > 0)
            {
                Entity ent = (Entity)stack.Pop();
                
                foreach (Entity child in ent.LinkedEntities)
                {
                   if (predicate(child))
                    return child;
                   stack.Push(child);
                }
            }
            return null;
        }

      
        public List<Entity> LinkedEntities {get;set;}
        
    }
    
   
}