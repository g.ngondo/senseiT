using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace  senseiT.Core.Shared
{
    public abstract class BaseEntity
    {
        public BaseEntity()
        {
            Id = Guid.NewGuid();
        }
        public Guid Id {get;set;}
        public List<BaseDomainEvent> Events = new List<BaseDomainEvent>();
        public string Name { get; set; }
        public EntityType Type {get;set;}
        public EntityStatus Status {get;set;}
        public string Url { get; set; }    
        public int Level {get;set;}
        public int Weight {get;set;} = 0;
        
    }
    public enum EntityStatus {
            Created,
            Added,
            Updated,
            Selected,
        }
        public enum EntityType {
            Root,
            Topic,
            Category,
        }
    
}