using System;

namespace  senseiT.Core.Shared
{
    public abstract class BaseDomainEvent {
        public DateTime DateOccurred { get; protected set; } = DateTime.UtcNow;
    }
}