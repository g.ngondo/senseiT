using Xunit;
using System.Net.Http;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;

using senseiT.Web;
using CleanArchitecture.Tests.Integration.Web;

namespace senseiT.Tests.Web
{
    public class EntityControllerTest : BaseWebTest
    {
       
        [Fact]
        public async void EntityControllerTestListReturnCorrectEntities()
        {
            // Act
            var response = await _client.GetAsync("/api/Entity/");
            response.EnsureSuccessStatusCode();

            var responseString = await response.Content.ReadAsStringAsync();

            // Assert
            Assert.Equal("{}", responseString);
        }
    }
}
