using senseiT.Web;
using Microsoft.AspNetCore.Hosting;

using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.IO;
using System.Net.Http;
using System.Reflection;

namespace CleanArchitecture.Tests.Integration.Web
{
    public abstract class BaseWebTest
    {
        protected readonly HttpClient _client;

        public BaseWebTest()
        {
            _client = GetClient();
        }
        

        protected HttpClient GetClient()
        {
            var startupAssembly = typeof(Startup).GetTypeInfo().Assembly;
            
           
            var builder = new WebHostBuilder()
                .UseKestrel()
                .UseContentRoot(Directory.GetCurrentDirectory())
                .UseStartup<Startup>(); // ensure ConfigureTesting is called in Startup

            var server = new TestServer(builder);
            var client = server.CreateClient();

            return client;
        }

       
        

    }
}