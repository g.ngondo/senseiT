using Xunit;
using senseiT.Core.Services;
using senseiT.Core.Entities;
using System.Linq;
using System.Collections.Generic;
using System;

namespace senseiT.Tests.UnitTests
{
    public class MemoryEntityRepositoryTest
    {
        
        [Fact]
        public void QueryEntities()
        {
            Entity ent1 = new Entity("Programming language",EntityType.Category);
            Entity ent2 = new Entity("Plateforms",EntityType.Category);

            Dictionary<Guid,Entity> entities = new Dictionary<Guid,Entity> {
                {ent1.Id,ent1},
                {ent2.Id, ent2},
            };

            MemoryEntityRepository repo = new MemoryEntityRepository(entities);
            Assert.Equal(entities.Count, repo.GetEntities().Count());
        }

        [Fact]
        public void AddEntity()
        {
           MemoryEntityRepository repo = new MemoryEntityRepository();
           Entity ent1 = new Entity("Programming language",EntityType.Category);
           Entity ent2 = new Entity("Plateforms",EntityType.Category);
           
            int currentCount = repo.GetEntities().Count();
            repo.AddEntity(ent1);
            repo.AddEntity(ent2);

            Assert.Equal(currentCount + 2, repo.GetEntities().Count());
        }
    }
}
